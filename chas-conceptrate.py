#!python

# Usage example:
# python chas-conceptrate.py nicegirl_dataset results 42

from PIL import Image
from random import randint
import sys
import os

source_dir = sys.argv[1]
print("Processing directory: " + source_dir)

target_dir = sys.argv[2]
print("Putting to directory: " + target_dir)

repeats = sys.argv[3]
print("Every image will be repeated " + repeats + " time(s)")
repeats = int(repeats)

try:
	os.mkdir(target_dir)
except:
	print("Could not create directory: " + target_dir)
	print("It may exist already, and then that's OK. The files in it will not be deleted but may be overwritten.")
	print("Proceeding anyway...")








def process_image(filename, number_of_copies, allow_flip = True):
	print("Processing file: " + filename + "  ...")
	try:
		image = Image.open(source_dir + "/" + filename)
		width, height = image.size
		print("Width:", width)
		print("Height:", height)

		for i in range(0, number_of_copies):
			left_shift = randint(0,  width - 512)
			top_shift  = randint(0, height - 512)

			cropped_image = image.crop( (left_shift, top_shift, left_shift + 512, top_shift + 512) )

			if randint(0,1) and allow_flip:
				cropped_image = cropped_image.transpose(Image.Transpose.FLIP_LEFT_RIGHT)
				is_flipped = "flip"
			else:
				is_flipped = "noflip"

			cropped_image.save(
				target_dir + "/" +
				str(i) + "_" +
				filename + "___" +
				str(left_shift) + "_" +
				str(top_shift) + "_" +
				is_flipped + "_cropped.png"
			)
	except:
		print('Unable to process image: ' + filename)


images = []

for path in os.listdir(source_dir):
	# check if current path is a file
	if os.path.isfile(os.path.join(source_dir, path)):
		process_image(path,repeats)



